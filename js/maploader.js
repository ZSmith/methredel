



/* -------------------------------------------------------- */
/* ------------------ Main Load Function ------------------ */
/* -------------------------------------------------------- */

function InitializeMap(mapInfo) {
    console.log("Initializing map! Mapinfo:");

    for (var k in mapInfo) {
        var location = mapInfo[k];
        AddLocation(location)
    }

}

// Adds a location to the map
// Create a panel and assign relevant onclick events,
// based on data loaded by jekyll
function AddLocation(location) {
    console.log("Adding a location!")
    for (var i in location) {
        console.log("Key: "+i+", Value: "+location[i]);
    }

    var locationPanel = document.createElement("div");
    var locationTooltip = document.createElement("div");

    var mapContainer = document.getElementById("mapContainer");
    var descriptionTitle = document.getElementById("descriptionTitle");
    var descriptionText = document.getElementById("descriptionText");

    var normalBackgroundImage = "url('../../images/map-components/position-marker.png')";
    var hoverBackgroundImage = "url('../../images/map-components/position-marker-hover.png')";

    mapContainer.appendChild(locationPanel);
    locationPanel.classList.add("mapLocation");
    locationTooltip.classList.add("mapTooltip");

    // Set location
    locationPanel.style.left = location.left;
    locationPanel.style.top = location.top;
    locationPanel.style.backgroundImage = normalBackgroundImage;

    // Assign tooltip
    locationTooltip.textContent = location.name;
    locationTooltip.style.visibility = "hidden";
    locationPanel.appendChild(locationTooltip);


    // On click: Set description box
    locationPanel.onclick = function() {
        console.log("Clicked location: "+location.name);
        descriptionTitle.textContent = location.name;
        descriptionText.textContent = location.description;
    }

    // On mouse over: show tooltip and hover effect
    locationPanel.onmouseover = function() {
        console.log("Mouse over location: "+location.name);
        locationTooltip.style.visibility = "visible";
        locationPanel.style.backgroundImage = hoverBackgroundImage;
    }

    // On mouse out: undo!
    locationPanel.onmouseout = function() {
        locationTooltip.style.visibility = "hidden";
        locationPanel.style.backgroundImage = normalBackgroundImage;
    }


}

// Periodically check the position of the mouse over the map
// Display in a chat box somewhere this position
// Should make it easier to add locations

// Get X and Y position of the elm (from: vishalsays.wordpress.com)
function getXYpos(elm) {
  x = elm.offsetLeft;        // set x to elm’s offsetLeft
  y = elm.offsetTop;         // set y to elm’s offsetTop

  elm = elm.offsetParent;    // set elm to its offsetParent

  //use while loop to check if elm is null
  // if not then add current elm’s offsetLeft to x
  //offsetTop to y and set elm to its offsetParent
  while(elm != null) {
    x = parseInt(x) + parseInt(elm.offsetLeft);
    y = parseInt(y) + parseInt(elm.offsetTop);
    elm = elm.offsetParent;
  }

  // returns an object with "xp" (Left), "=yp" (Top) position
  return {'xp':x, 'yp':y};
}

// Get X, Y coords, and displays Mouse coordinates
function getCoords(e) {
 // coursesweb.net/
  var xy_pos = getXYpos(this);

  // if IE
  if(navigator.appVersion.indexOf("MSIE") != -1) {
    // in IE scrolling page affects mouse coordinates into an element
    // This gets the page element that will be used to add scrolling value to correct mouse coords
    var standardBody = (document.compatMode == 'CSS1Compat') ? document.documentElement : document.body;

    x = event.clientX + standardBody.scrollLeft;
    y = event.clientY + standardBody.scrollTop;
  }
  else {
    x = e.pageX;
    y = e.pageY;
  }

  x = x - xy_pos['xp'];
  y = y - xy_pos['yp'];

  var h = document.getElementById('mapContainer').offsetHeight;
  var w = document.getElementById('mapContainer').offsetWidth;

  x = (x/w*100).toFixed(2);
  y = (y/h*100).toFixed(2);

  // displays x and y coords in the #coords element
  document.getElementById('locationLabel').innerHTML = 'X: '+ x+ ', Y: ' +y;
}


/* -------------------------------------------------------- */
/* -------------------------- IIFE ------------------------ */
/* -------------------------------------------------------- */


window.onload = function() {
    InitializeMap(mapInfo);

    document.getElementById("mapContainer").onmousemove = getCoords;
}

var x, y = 0;       // variables that will contain the coordinates
