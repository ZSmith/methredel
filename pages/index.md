---
title: Active Plots
show_sidebar: True
layout: default
permalink: /
---

## Active Plots

* The country is at war! Without the airship to aid in proactive engagements, combat is restricted to the front lines.

* The Convergence is here. Noone really knows what that means, though.

* After the death of Zeras, the Archaic Order seems to have retreated into the background. It seems their plans were foiled, but what are they really trying to achieve?

* A bizarre masked figure, Jhin, has started some open "performances" in town -- with dangerous consequences.

* Roxy has a scheduled court appearance, and the consequences for failing to appear may be dire. 