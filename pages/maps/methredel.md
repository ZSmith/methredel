---
title: The Land of Methredel
show_sidebar: True
layout: map

map: methredel-v1.png
mapWidth: 1920px
mapHeight: 1080px

locations:
  - name: The Volcano
    left: 65%
    top: 44.5%
    description: |
        A long dormant volcano now has faint smoke coming from the top.
  - name: Excelsior's Boutique
    left: 45%
    top: 37.5%
    description: |
        The Alchemist Excelsior is always willing to brew some potions -- if provided with the right supplies.
  - name: The Swamp Goblins
    left: 30%
    top: 28%
    description: |
        These goblins have been studying the art of indirect diplomacy.
  - name: The Ruins
    left: 22%
    top: 45%
    description: |
        Supposedly haunted ruins of a forgotten city may be related to the Convergence.
  - name: Kabuki's Estate
    left: 32%
    top: 44%
    description: |
        Once a fine mansion, now in a state of disrepair after repeated visits from Freddy the Flint and other vandals.

        And the owner is nowhere to be seen...
  - name: The Front Line
    left: 65%
    top: 36.5%
    description: |
        While people cling to normalcy in the city, the war has begun on the front lines.
  - name: The Academy
    left: 46%
    top: 13%
    description: |
        The remaining leaders of the Academy sit in an empty tower, their students claimed by the military, and their presence unwelcome elsewhere.
  - name: Leffen's Court
    left: 34%
    top: 49%
    description: |
        No reason to go here, unless you've broken some laws?
  - name: Military Camp
    left: 37%
    top: 42%
    description: |
        A military camp has been opened outside the city's edge -- what are they working on in there?





---
